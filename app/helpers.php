<?php


function flash($text='A test Notification',$type='info')
{
    session()->flash('text',$text);
    session()->flash('type',$type);
}