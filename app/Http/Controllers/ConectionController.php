<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ConectionController extends Controller
{
    public static function createDB($dbname)
    {
        try
        {
            $cmdResponse    =   DB::statement('create database '.$dbname );
        }
        catch (\Exception $exception)
        {
            return dd($exception);
            return response()->json(['message'=>'Unable to create database','status'=>'error'],400);
        }

        if($cmdResponse===TRUE)
        {
            try
            {
                $new_connection = 'new';
                \Illuminate\Support\Facades\Config::set('database.connections.'.$new_connection, array(
                    'driver'    => 'mysql',
                    'host'      => '127.0.0.1:3306',
                    'database'  => $dbname,
                    'username'  => 'root',
                    'password'  => '',
                ));
                Artisan::call('migrate', ['--database' => $new_connection]);
                return response()->json(['message'=>'Database created and migration run successfully','status'=>'success'],201);
            }
            catch (\Exception $exception)
            {
                return dd($exception);
                return response()->json(['message'=>'Unable to create database','status'=>'error'],400);
            }
        }
        else
        {
            return response()->json(['message'=>'Unable to create database','status'=>'error'],400);
        }

    }

    public static function connectDB($db)
    {
        DB::purge('database.connections.mysql');
        DB::disconnect();
        Config::set('database.connections.mysql.database',$db);
        DB::reconnect();
    }
}
