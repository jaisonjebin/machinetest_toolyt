<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('custom_auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->type=='admin')
        {
            $users  =   User::where('id','!=',Auth::user()->id)->get();
            return view('home',compact('users'));
        }
        else
        {
            return view('home');
        }
    }

    public function userStatus($user)
    {
        $user   =   User::find($user);
        if($user)
        {
            $user->status   =   !($user->status);
            $user->save();
            return response()->json(['url'=>route('userStatus',$user->id),'message'=>($user->status?'Enable':'Disable'),'status'=>'success'],200);
        }
        else
        {
            return response()->json(['message'=>'User not found','status'=>'error'],404);
        }

    }
}
