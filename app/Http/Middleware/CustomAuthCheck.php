<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CustomAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Session::exists('db')) {
            DB::purge('database.connections.mysql');
            DB::disconnect();
            Config::set('database.connections.mysql.database',Session::get('db'));
            DB::reconnect();
        }

        if(Auth::check())
        {
            return $next($request);
        }
        return redirect('/login');
    }
}
