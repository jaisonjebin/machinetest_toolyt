<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ConectionController;
use App\User;
use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class SwitchDB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        DB::purge('database.connections.mysql');
        DB::disconnect();
        Config::set('database.connections.mysql.database','laravel');
        DB::reconnect();
        $user   =   User::where('email',$request->email)->where('status',1)->first();
        if(!$user)
        {
            return redirect('login')->with('status', 'User not found');
        }
        else
        {
            if($user->type=='admin')
            {
                ConectionController::connectDB('laravel');
            }
            else
            {
                ConectionController::connectDB($user->database_name);
            }

            return $next($request);
        }

    }
}
