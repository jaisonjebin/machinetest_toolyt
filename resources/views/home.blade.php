@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        {{--You are logged in!--}}
                    @if(isset($users))
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th> ID</th>
                                    <th> NAME</th>
                                    <th> EMAIL </th>
                                    <th> STATUS</th>
                                    {{--<th> ACTION</th>--}}
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th> ID</th>
                                    <th> NAME</th>
                                    <th> EMAIL </th>
                                    <th> STATUS</th>
                                    {{--<th> ACTION</th>--}}
                                </tr>
                                </tfoot>
                                <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td> {{$user->id}} </td>
                                <td> {{$user->name}} </td>
                                <td> {{$user->email}} </td>
{{--                                <td class="status"> {{($user->status==1)?'Active':'Inactive'}} </td>--}}
                                <td> <a href="javascript:void(0);" data-href="{{route('userStatus',$user->id)}}" class="changeStatus"><label>{{($user->status==1)?'Enable':'Disable'}}</label></a> </td>
                            </tr>
                        @endforeach
                                </tbody>
                            </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function() {
            $('.changeStatus').click(function(e) {
                var ele=   $(this);
                var linkURL = ele.data("href");

                $.ajax({
                    type: "GET",
                    url: linkURL,
                    dataType:'json',
                    success: function( msg ) {
                        if(msg.status=='success')
                        {
                            ele.closest('tr').find('.changeStatus').html(msg.message);
                            ele.closest('tr').find('.changeStatus').data('href',msg.url);
                            toastr["success"]("User Status Updated");
                        }
                        else
                        {
                            toastr["warning"]("Failed to update user status");
                        }
                    },
                    error: function (jqXHR, exception)
                    {
                        toastr["warning"]("Failed to update user status");
                    }
                });
            });
        });

    </script>
@endsection
